#Airplane object class

class Airplane:

	def __init__(self, name, sub_time, request_time, duration):
		
		self.name = name
		self.sub_time = int(sub_time)
		self.request_time = int(request_time)
		self.duration = int(duration)
