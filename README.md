Python Traffic Control Simulator - Jackson Nelson

This software will simulate an airplane take off schedule for an airport. It will keep track of the
requesting airplane, the submission time of the request, the time slot requested, the duration of 
take off, the actual start time and actual end time. This will be accomplished by creating a priority
queue that tracks the requested blocks of time for take off.

This folder will contain one Python file for handling the creation and ordering of the priority queue,
one file that handles the submission of an input file with a runway schedule, and more.

Author: Jackson Nelson. nelsonj6@augsburg.edu