from queue import PriorityQueue
from Airplane import Airplane
from pqueue import PQueue
import sys

class runway:

    queue = PriorityQueue()

    """
    This function opens a specified file and reads the data into a priority queue.
    """
    def read_file(userFile):
    
        #Input file
        in_file = open(userFile, 'r')

        plane_strings = in_file.readlines()

        #Remove newline chars
        for i in range (len(plane_strings)):
            line = plane_strings[i].rstrip()
            del plane_strings[i]
            plane_strings.insert(i, line)
            
        return plane_strings

    """
    This function creates a nice list of strings that we will use to build planes.
    """
    def create_planes(string_of_planes):
    
        # A place to store airplane objects
        airplane_objects = []

        # For each item in the string list of planes, make and store a plane object
        for i in range (len(string_of_planes)):
            # Look at a string
            plane = string_of_planes[i]
            # Split the parts
            temp = plane.split(',')
            # Look at name, sub time, request time, duration
            name = temp[0]
            submit = temp[1]
            request = temp[2]
            duration = temp[3]
            # Make an object from strings
            airplane = Airplane(name, submit, request, duration)
            # Put it in an array
            airplane_objects.append(airplane)
        #print(airplane_objects[0].name, airplane_objects[1].name, airplane_objects[2].name)
        return airplane_objects
        

    """
    This function runs the time simulator
    """
    def run_time(airplane_array):
    
        queue = PQueue()
        print("Start of the day")
        time = 0

        # Track the day
        while time < 24:
            for i in range (len(airplane_array)):
                # If the request is now, add to queue
                if (airplane_array[i].sub_time == time):
                    queue.recur_enqueue(airplane_array[i], 0)
                # if time to leave, dequeue
                if (time >= (airplane_array[i].request_time + airplane_array[i].duration)):
                    queue.dequeue()
            print("Queue at time ", time, ":", queue.queue_status())
            time += 1
        return

    if __name__ == "__main__":
        file = sys.argv[1]
        #file = "runwayLog.txt"
        list = read_file(file)
        array = create_planes(list)
        run_time(array)

