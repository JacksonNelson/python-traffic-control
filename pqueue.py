from Airplane import Airplane



class PQueue:

    """
    This function is a constructor
    """
    def __init__(self):
        self.queue = []

    """
    This function is checks if something exists at 0, if so removes it
    """
    def dequeue(self):
        if len(self.queue) > 0:
            self.queue.pop(0)

    """
    This function returns size
    """
    def size(self):
        return len(self.queue)
            

    """
    This function enqueues items. First it checks if the plane already exists,
    and won't add it if so. Then checks if queue is empty, adds to end if so.
    Then check for equal request times, sort by sub time if so.
    Then check if request is greater than index time, try on next position if so.
    Then check if request is less than index time, try previous if so.
    """
    def recur_enqueue(self, plane: Airplane, position: int):
        length = len(self.queue)
        
        for x in range (length):
            if plane.name == self.queue[x].name:
                return
        # If queue is empty, just add to end
        if length == 0:
            #print("Empty!")
            self.queue.append(plane)
            #print("Inserted.")
        #Otherwise, we have to compare.
        else:
            #Iterate over queue
            for i in range (position, length):
                
                # Is the current request equal to another request?
                if plane.request_time == self.queue[i].request_time:
                    #print("Equal!")
                    # If so, compare sub times: lower sub goes first
                    if plane.sub_time < self.queue[i].sub_time:
                        self.queue.insert(i, plane)
                        #print("Inserted.")
                    else:
                        self.queue.insert((i + 1), plane)
                        #print("Inserted.")
                # is the current request greater than other requests?
                elif plane.request_time > self.queue[i].request_time:
                    #print("Too big!")
                    #if we're at the end, put plane at end
                    if i == (length - 1):
                        self.queue.append(plane)
                        #print("Inserted.")
                    #otherwise, try putting at next position
                    else:
                        position = position + 1
                        self.recur_enqueue(plane, position)
                # Is the current request smaller than other requests?
                elif plane.request_time < self.queue[i].request_time:
                    #print("Too small!")
                    # If we're at the beginning, put at front
                    if position == 0:
                        self.queue.insert(position, plane)
                    #otherwise, try previous position
                    else:
                        position = position - 1
                        self.recur_enqueue(plane, position)
                else:
                    print("Error!")

    """
    This function returns a plane at specified position
    """
    def get_plane(self, index):
        return self.queue[index]

    """
    This function returns the status of the queue
    """
    def queue_status(self):
        status = ""
        for i in range (len(self.queue)):
            status = status + self.get_plane(i).name + ","
        return status
